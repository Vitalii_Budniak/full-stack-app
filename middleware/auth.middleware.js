const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) => {
    if (req.method === 'OPTIONS') return next();
    console.log("Request Method:", req.method)
    try {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(401).json({message: "Not authorized"})
        }
        const decoded = jwt.verify(token, config.get('jwtSecret'));
        req.user = decoded;
        next();

    } catch (e) {
        res.status(401).json({message: "Not authorized"})
    }
};

// function decode() {
//     const token = "$2a$12$VOpZu.LygMm9qvggauBjJul/rXwoTDfDzBf0wXZo1d9HtpbfHzGa.";
//     const jwtSecret = "vitalii full stack";
//     const decoded = jwt.verify(token, config.get('jwtSecret'));
//     console.log(decoded)
// }
//
// decode()
