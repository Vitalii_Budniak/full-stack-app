const  { Router } = require('express');
const Link = require('../models/Link');
const router = Router();
const auth = require('../middleware/auth.middleware');
const config = require('config');
const shortId = require('shortid');


//Add middleware auth into get endpoint for checkout request
router.post('/generate', auth, async (req, res) => {
    try {
        const baseUrl = config.get('baseUrl');
        const { from } = req.body;

        const code = shortId.generate();

        const existing = await Link.findOne({ from });

        if (existing) {
            return res.json({ link:existing });
        }

        const to = baseUrl + '/' + code;

        console.log("!!!!!!!!!!!", req.user)

        const link = new Link({
            code, to, from , owner: req.user.userId
        });

        await link.save();
        res.status(201).json({ link })


    } catch (e) {
        res.status(500).json({message: 'Something wrong with server'})
    }
});


//Add middleware auth into get endpoint for checkout request
router.get('/', auth, async (req, res) => {
    try {
        const links = await Link.find({ owner: req.user.userId }); //??
        res.json(links)
    } catch (e) {
        res.status(500).json({message: 'Something wrong with server'})
    }
});

router.get('/:id', auth, async (req, res) => {
    try {
        const link = await Link.findById(req.params.id);
        res.json(link)
    } catch (e) {
        res.status(500).json({message: 'Something wrong with server'})
    }
});

module.exports = router;
