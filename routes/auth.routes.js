const  { Router } = require('express');
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');

const User = require('../models/User');
const router = Router();


// prefix - /api/auth
router.post('/register',
    [
        check('email', 'Wrong email').isEmail(),
        check('password', 'Minimum password length is 6 symbols').isLength({ min: 6 }),
    ],
    async (req, res)=> {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Wrong registration data'
                })
            }
            const { email, password } = req.body;

            const candidate = await User.findOne({ email:email });
            if (candidate) {
                return res.status(400).json({message: 'Email already exist'})
            }

            const hashedPassword = await bcrypt.hash(password, 12);
            const user = new User({ email: email, password: hashedPassword});
            await user.save();
            res.status(201).json({message: 'User created'})

        } catch (e) {
            res.status(500).json({message: 'Something wrong with server'})
        }
});

router.post('/login',
    [
        check('email', 'Enter correct email').normalizeEmail().isEmail(),
        check('passwords', 'Enter password').exists(),
    ],
    async (req, res)=> {
        try {
            const errors = validationResult(req.body);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Wrong login data'
                })
            }

            const { email, password } = req.body;
            const user = await User.findOne({email});
            if (!user) {
                return res.status(400).json({
                    message: 'User not found'
                })
            }

            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) {
                return res.status(400).json({
                    message: 'Wrong password'
                })
            }

            const token = jwt.sign({ userid: user.id }, config.get('jwtSecret'), { expiresIn: '1h' });
            res.json({ token, userId: user.id })

        } catch (e) {
            res.status(500).json({message: 'Something wrong with server'})
        }

});

module.exports = router;
