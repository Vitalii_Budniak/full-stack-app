import React, {useContext, useState} from "react";
import { useHttp } from '../hooks/http.hook';
import {AuthContext} from "../context/AuthContext";
import { useHistory } from 'react-router-dom'

export const CreatePage = () => {
    const history = useHistory();
    const { token } = useContext(AuthContext);
    const [link, setLink] = useState('');

    const { request } = useHttp();

    const pressHanlder = async (event) => {

        if (event.key === 'Enter') {
            try {
              const data = await request('/api/link/generate', 'POST', { from: link }, { Authorization: `Bearer ${token}`});
              history.push(`/detail/${data.link._id}`)
            } catch (e) {
                console.log(e)
            }
        }

    };

    return (
        <div className="row">
            <div className="col s8 offset-s2" style={{paddingTop: '2rem'}}/>
            <div className="input-field">
                <input
                    placeholder="Enter URL"
                    id="link"
                    type="text"
                    value={link}
                    onChange={ event => setLink(event.target.value) }
                    onKeyPress={pressHanlder}
                />
                <label htmlFor="link">Enter URL</label>
            </div>
        </div>
    );
};
