import {useState, useCallback, useEffect} from 'react';
const jwt = require('jsonwebtoken');

export const useAuth = () => {
    const [token , setToken] = useState(null);
    const [ready , setReady] = useState(false);
    const [userId , setUserId] = useState(null);

    const login = useCallback( (jwtToken, id) => {
        setToken(jwtToken);
        setUserId(id);
        localStorage.setItem('userData', JSON.stringify({ token: jwtToken, userId: id }));
    }, []);

    const logout = useCallback( () => {
        setToken(null);
        setUserId(null);
        localStorage.removeItem('userData');
    }, []);

    useEffect(  () => {
        const data = JSON.parse(localStorage.getItem('userData'));
        if (data && data.token) {
            jwt.verify(data.token, 'vitalii full stack', function(err, decoded) {
                if (err) {
                    logout();
                } else if (decoded) {
                    login(data.token, data.userId)
                }
            });
        }
        setReady(true);
    }, [login, logout]);




    return { login, logout, token, userId, ready }
};