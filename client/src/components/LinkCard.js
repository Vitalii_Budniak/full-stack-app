import React from "react";



export const LinkCard = ({link}) => {
    return (
        <>
            <h2>Link</h2>
            <p>Your link to: <a href={link.to} target="_blank" rel="noopener norefferer">{link.to}</a> </p>
            <p>From: <a href={link.from} target="_blank" rel="noopener norefferer">{link.from}</a> </p>
            <p>Click count: <strong>{link.clicks} </strong> </p>
            <p>Created time: <strong>{new Date(link.date).toDateString()} </strong> </p>
        </>
    )
};