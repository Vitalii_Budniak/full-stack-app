import React from 'react';
import 'materialize-css';
import { BrowserRouter } from "react-router-dom"
import { useRoutes } from "./routes";
import { useAuth } from "./hooks/auth.hook";
import { AuthContext } from "./context/AuthContext";
import { NavBar } from "./components/NavBar";
import {Loader} from "./components/Loader";

function App() {

    const { login, logout, token, userId, ready } = useAuth();

    const isAuthenticated = !!token;
    const routes = useRoutes(isAuthenticated);

    if (!ready) {
       return <Loader/>
    }
    return (
      <AuthContext.Provider value = { { login, logout, token, userId, isAuthenticated } }>
          <BrowserRouter>
              {isAuthenticated && <NavBar/>}
              <div className='container'>
                  {routes}
              </div>
          </BrowserRouter>
      </AuthContext.Provider>
    );
}

export default App;


// https://www.youtube.com/watch?v=ivDjWYcKDZI&t=834s
// 2-55-00
